variable "hcloud_token" {}

variable "name" {
  type = string
  description = "Server name" 
}

variable "image" { 
  type = string
  default = "ubuntu-20.04"
}

variable "server_type" { 
  type = string
  default = "cx21"
}

variable "location" { 
  type = string
  default = "hel1"
}

variable "username" {
    description = "Main user name"
}

variable "subdomain" {
  type = string
  description = "Subdomain pointing to this server"
}
